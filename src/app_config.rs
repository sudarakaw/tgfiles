// src/app_config.rs: runtime configuration module
//
// Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

use std::path::PathBuf;

use anyhow::{Context, Result};
use config::{Config, Environment, File, FileFormat};
use serde::Deserialize;

use crate::{TelegramConfig, RCFILE};

#[derive(Debug, Default, Deserialize)]
pub struct AppConfig {
    pub files: Vec<PathBuf>,
    pub telegram: TelegramConfig,
}

impl AppConfig {
    pub fn from(filename: &str) -> Result<Self> {
        let cfg = Config::builder()
            .add_source(
                File::with_name(RCFILE)
                    .required(false)
                    .format(FileFormat::Yaml),
            )
            .add_source(
                File::with_name(filename)
                    .required(true)
                    .format(FileFormat::Yaml),
            )
            .add_source(Environment::with_prefix("tgfiles").separator("_"))
            .build()
            .with_context(|| "Faild to load configuration from file or environment.")?;

        cfg.try_deserialize::<Self>()
            .with_context(|| "Failed to decode configuration data")
    }
}
