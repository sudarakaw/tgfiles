// src/lib.rs: main module for the application
//
// Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

pub mod package;
use anyhow::Result;
pub use package::*;

pub mod app_config;
pub use app_config::AppConfig;

pub mod telegram;
pub use telegram::*;

pub fn run(cfg: &AppConfig) -> Result<()> {
    for f in &cfg.files {
        match f.metadata() {
            Ok(meta) => {
                if meta.is_file() && 0 < meta.len() {
                    send_file(f, &cfg.telegram)?;
                } else {
                    eprintln!("{}: not a file or is empty.", f.to_string_lossy());
                }
            }
            Err(err) => eprintln!("{}: {}", f.to_string_lossy(), err),
        }
    }

    Ok(())
}
