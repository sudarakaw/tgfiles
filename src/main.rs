// src/main.rs: main entry point for the executable program.
//
// Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

use anyhow::Result;
use clap::Parser;
use tgfiles::{run, AppConfig, RCFILE};

#[derive(Parser, Debug)]
#[clap(author, version, about)]
struct Args {
    #[clap(
        long,
        short,
        value_name = "FILE",
        help = "Custom configuration file",
        default_value = RCFILE
    )]
    config: String,
}

fn main() -> Result<()> {
    let args = Args::parse();
    let cfg = AppConfig::from(&args.config)?;

    run(&cfg)?;

    Ok(())
}
