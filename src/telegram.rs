// src/config/telegram.rs: Telegram configuration module
//
// Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

use std::path::Path;

use anyhow::{bail, Context, Result};
use reqwest::{
    blocking::{multipart::Form, Client},
    StatusCode,
};
use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Deserialize)]
pub struct TelegramConfig {
    pub api_key: String,
    pub chat_id: u32,
}

#[derive(Debug, Serialize)]
struct TgMessage<'a> {
    chat_id: u32,
    parse_mode: &'a str,
    document: &'a Path,
}

#[derive(Debug, Deserialize)]
struct ResponseBody {
    description: String,
}

pub fn send_file(file: &Path, cfg: &TelegramConfig) -> Result<()> {
    let api_url = format!("https://api.telegram.org/bot{}/sendDocument", cfg.api_key);
    let http_client = Client::new();
    let form = Form::new()
        .text("chat_id", format!("{}", cfg.chat_id))
        .text("caption", format!("*From: *`{}`", file.to_string_lossy()))
        .text("parse_mode", "markdown")
        .file("document", file)?;

    let response = http_client
        .post(api_url)
        .multipart(form)
        .send()
        .with_context(|| "Sending request to Telegram API")?;

    if StatusCode::OK != response.status() {
        let result = response
            .json::<ResponseBody>()
            .with_context(|| "Decoding response body")?;

        bail!(format!("API Error: {}", result.description));
    }

    Ok(())
}
